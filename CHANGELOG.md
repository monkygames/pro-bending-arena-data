# Pro Bending Arena Data Changelog

## 2021-01-11 - v0.1.8

* Fixed an issue with the tricks only containing Earth cards.

## 2020-09-22 - v0.1.7

* Optimized Team Cards.
* Fixed formatting issues on all Team Cards as the card name was out of position.
* Created a single PDF for printing Team Cards and now included in the 'downloads' section of bitbucket.
* Moved tricks into 3 separate files to make it easier to build a single pdf.

## 2020-09-15 - v0.1.6

* Changed DPI from 96 to 300 on tricks SVG which is better for printing.

## 2020-09-15 - v0.1.5

* Uploaded the Readme and Changelog to comply with Markdown style.
* Added All Trick Cards for use by Teams

## 2019-02-26 - v0.1.4

  - Added Trick Cards (all trick cards except tricks that came with teams).
  - Optimized SVG Files

## 2019-02-12 - v0.1.3
  - Optimized SVG files
  - Added Team Packets

## 2019-02-12 - v0.1.2
  - Added printable map

## 2019-02-11 - v0.1.1
  - Added Bore-Q-Pines
  - Added Buzzard Wasps
  - Updated card template

## 2019-02-04 - v0.1.0
  - Changed component's allignment in  card template.
  - Added Rabaroos.
