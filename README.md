# Introduction
This project contains data for [IDW](https://idwgames.com)'s [The Legend of Korra: Pro-Bending Arena](https://idwgames.com/shop/the-legend-of-korra-pro-bending-arena) tabletop board game.

## Cards
The cards directory contains assets related to the pro-bending arena cards.  Team Names are used for directories that contain cards related to a team.

The following is the directory contents for a team.

### Team Name
* Earth
* Fire
* Water
* Team Name.svg
* printable_A4_page1.svg
* printable_A4_page2.svg

The earth, fire, and water directories contain an svg for the respective cards.  The card formats are 41mm x 63mm and are designed to fit FFG's [Mini American Card Sleeves](https://www.fantasyflightgames.com/en/products/fantasy-flight-supply/products/mini-american-board-game-sleeves/).  The cards are simplified to reduce card size and also to reduce ink.  Each team has its name.svg which is a simplified version of the team dashboard.  The size of the team dashboard is the same as other cards (41mm x 63mm).

### Assets
The assets directory contains files to aid in building teams.  

### Building a team

* Create a new directory with the team name in the cards directory.
* Copy assets/team_template.svg to new directory and rename file to the name of the team.
* Create fire, earth, and water directories.
* Copy assets/card_template.svg to create cards in the respective directory.



## Tricks

The tricks directory contains assets related to the trick cards.  The expansion pack names are used for directories.  Each directory contains trick cards from the expansion.  Core is the 'core' set.  Note, most of the trick cards were Kickstarter exclusive only.

The Tricks directory contains the following files:

* tricks_printable_A4.svg
* trick_template.svg

The tricks_printable_A4.svg is a vector file includes all Trick Cards.  There are 3 layers in this file (Earth, Fire, and Water).  To print, just hid 2 out of the 3 layers and repeat for each Element.  Note, each card in this file is linked via SVG linking.  The source can be found in the directories.

The trick_template.svg is the template used to build new tricks.