#!/bin/bash

VERSION=0.1.8
VERSION_DIR="versions/${VERSION}"

# $1 src
# $2 dest
# $3 layer
function export_inkscape {
  inkscape --file $1 -i $3 --export-area-page --without-gui --export-pdf=$2
}

# $1 src
# $2 dest
function export_inkscape_nolayer {
  inkscape --export-filename $2 --export-area-page  $1 &> /dev/null
}


# Builds an array of layers from inkscape file.
# $1 src
function build_array {
  mapfile -t UNSORTED_LAYER_ARRAY < <(inkscape --query-all $1 | grep layer | awk -F, '{print $1}')

  # sort by layer name.
  LAYER_ARRAY=( $( printf "%s\n" "${UNSORTED_LAYER_ARRAY[@]}" | sort --version-sort ) )

}

# $1 Dir Name
function export_team {
  NAME=$1
  echo "Exporting ${NAME}"
  export_inkscape_nolayer "cards/${NAME}/printable_A4_page1.svg" "${VERSION_DIR}/${NAME}-p1.pdf"
  export_inkscape_nolayer "cards/${NAME}/printable_A4_page2.svg" "${VERSION_DIR}/${NAME}-p2.pdf"
}

function add_team {
  NAME=$1
  echo "Adding ${NAME}"
  UNITE_STRING+="${VERSION_DIR}/${NAME}-p1.pdf "
  UNITE_STRING+="${VERSION_DIR}/${NAME}-p2.pdf "
}

function build_teams {
  UNITE_STRING=''
  export_team "Bore-Q-Pines"
  export_team 'Buzzard_Wasps'
  export_team 'Fire_Ferrets'
  export_team 'Rabaroos'
  export_team 'Tigerdillos'
  export_team 'Wolfbats'

  add_team "Bore-Q-Pines"
  add_team 'Buzzard_Wasps'
  add_team 'Fire_Ferrets'
  add_team 'Rabaroos'
  add_team 'Tigerdillos'
  add_team 'Wolfbats'
  
  pdfunite $UNITE_STRING "versions/teams_A4-v${VERSION}.pdf"
}

function build_tricks {
  export_inkscape_nolayer "tricks/tricks_printable_A4-earth.svg" "${VERSION_DIR}/tricks-earth.pdf"
  export_inkscape_nolayer "tricks/tricks_printable_A4-fire.svg" "${VERSION_DIR}/tricks-fire.pdf"
  export_inkscape_nolayer "tricks/tricks_printable_A4-water.svg" "${VERSION_DIR}/tricks-water.pdf"

  UNITE_STRING="${VERSION_DIR}/tricks-earth.pdf ${VERSION_DIR}/tricks-fire.pdf ${VERSION_DIR}/tricks-water.pdf"
  pdfunite $UNITE_STRING "versions/tricks_A4-v${VERSION}.pdf"
}


if [ ! -d "${VERSION_DIR}" ] ; then
  mkdir -p ${VERSION_DIR}
fi


#build_array $TRICKS_FILE
#
#COUNT=1
#UNITE_STRING=""
#echo "STARTING FOR LOOP"
#for var in "${LAYER_ARRAY[@]}" ; do
#  PAGE=$(printf "%0*d" 2 "${COUNT}")
#  echo "Export the ${var} layer from the svg"
#  export_inkscape $TRICKS_FILE "versions/${VERSION}/${PAGE}.pdf" "${var}"
#  #echo 'Reduce the page size'
#  #reduce_pdf "../versions/${VERSION}/${PAGE}.pdf"
#  UNITE_STRING+="versions/${VERSION}/${PAGE}.pdf "
#
#  #increment counter
#  ((COUNT++))
#done
#
#echo 'Merge pages into single PDF'
#pdfunite $UNITE_STRING "versions/tricks_printable_A4_v${VERSION}.pdf"
#

build_teams
build_tricks
